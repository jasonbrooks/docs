= Base images

This project closely tracks CentOS and Fedora, and aims
to tightly integrate with their underlying lifecycle as
well as release and test infrastructure.  These are the
default "full" images:

- CentOS Stream: `{container-c9s}` (https://github.com/centos/centos-bootc[source repo])
- Fedora: `{container-fedora-full}` (https://gitlab.com/fedora/bootc/base-images[source repo])

== "full" image philosophy

The content set for the default images are intentionally quite large.

- `bootc`: Included in the container to perform in-place upgrades "day 2"
- `kernel`: There's a kernel
- `systemd`: systemd is included and is configured to run by default when executed as a container too
- `NetworkManager`: Full support for many complex networking types
- `podman`: Support for OCI containers
- Filesystem tools, support for LUKS, LVM, RAID etc.
- Lots of other supporting tools, such as `sos`, `jq` etc.

=== No cloud agents by default

However, the image does *not* include hypervisor specific agents. You may
install them in derived builds. For example, on Amazon Web Services, you may
want to install `cloud-init`; but this is also not required.

See xref:cloud-agents.adoc[Cloud agents] for more information.

== `minimal` image philosophy

There are also demonstration images that are much smaller that are intended
to be used more as a development reference. These images are not currently
published on Quay.io but are available in
registry.gitlab.com/fedora/bootc/base-images/fedora-bootc-minimal:40-amd64[the Gitlab container registry].

This image has almost nothing; just `kernel systemd bootc`; everything
else (including e.g. networking) you need to add.
