= Node counting

.NOTE: This content is specific to Fedora and does not currently apply to CentOS.

Fedora nodes are counted by the Fedora infrastructure via the Count Me feature. This system is explicitly designed to make sure that no personally identifiable information is sent from counted systems. It also ensures that the Fedora infrastructure does not collect any personal data. The nickname for this counting mechanism is "Count Me", from the option name. Implementation details of this feature are available in https://fedoraproject.org/wiki/Changes/DNF_Better_Counting[DNF Better Counting change request for Fedora 32]. In short, the Count Me mechanism works by telling Fedora servers how old your system is (with a very large approximation).

CentOS bootc does not include Count Me infrastructure.

== More information on Count Me

This system is explicitly designed to make sure that no personally identifiable information is sent from counted systems. It also ensures that the Fedora infrastructure does not collect any personal data. The nickname for this counting mechanism is "Count Me", from the option name. Implementation details of this feature are available in https://fedoraproject.org/wiki/Changes/DNF_Better_Counting[DNF Better Counting change request for Fedora 32]. In short, the Count Me mechanism works by telling Fedora servers how old your system is (with a very large approximation).

On {projname} in the `full` image, note that 3 different tools are included:

- `dnf`
- `bootc`
- `rpm-ostree` (currently)

=== At container build time

If you use `dnf` as part of a container build, it will inherently run through the `countme` process unless you set `countme=0` in `/etc/yum.repos.d/fedora/*.repo`.

=== At machine runtime

Because the base image includes `rpm-ostree`, it will run by default https://coreos.github.io/rpm-ostree/countme/[rpm-ostree as a stand-alone method]. The new implementation has the same privacy preserving properties as the original DNF implementation.

Additionally, if you invoke `dnf` at runtime on an installed system, it will also be counted unless
disabled in the `/etc/yum.repos.d` files.

=== Intersection with `dnf-makecache.timer`

The `dnf-makecache.timer` systemd unit would also by default invoke counting, except it is disabled
by default if `/run/ostree` is present, which suppresses the cache invocation by default.

=== Intersection with `bootc`

At the current time there is no default interaction between the Count Me infrastructure and `bootc`.

== Opting out of counting

You can disable the `countme` repo flags and the unit timer as part of a container build:

[source,dockerfile]
----
RUN sed -i -e s,countme=1,countme=0, /etc/yum.repos.d/*.repo && systemctl mask --now rpm-ostree-countme.timer
----

